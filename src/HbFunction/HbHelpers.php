<?php

/**
 * 辅助函数库
 * @date    2023-04-14 15:18:43
 */


use Gzhb\HbBaseTool\HbConstant\EnvConstant;
use Gzhb\HbBaseTool\HbConstant\TipsConstant;


if (!function_exists('isTestEnv')) {
    /**
     * 是否测试环境
     */
    function isTestEnv(): bool
    {
        ## .env 文件校验
        $env = env('APP_ENV', EnvConstant::APP_ENV_DEV);
        return ($env == EnvConstant::APP_ENV_PRD) ? false : true;
    }
}


if (!function_exists('isHttps')) {
    /**
     * 是否是https协议请求来源 
     */
    function isHttps()
    {
        return ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? true : false;
    }
}

if (!function_exists('json_success_array')) {
    /**
     * API成功的信息返回-array
     * @param  mixed $data 数据
     * @param  string $msg  提示文案
     */
    function json_success_array(
        $data = [],
        $msg = TipsConstant::OK,
        $errors = [],
        $code = 200
    ) {
        ## 成功
        return [
            'code'   => $code,
            'status' => true,
            'msg'    => $msg,
            'res'    => $data,
            'errors' => $errors
        ];
    }
}

if (!function_exists('json_error')) {
    /**
     * API错误的返回信息 (旧方法)
     * @param  mixed $msg  
     * @param  mixed  $data 
     * @param  mixed $code 
     */
    function json_error(
        $msg = TipsConstant::FAIL,
        $data = [],
        $errors = [],
        $code = -1
    ) {
        return response()->json([
            'code'   => $code,
            'status' => false,
            'msg'    => $msg,
            'res'    => $data,
            'errors' => $errors,
        ]);
    }
}

// if (!function_exists('json_error_arr')) {
//     /**
//      * API错误的返回信息-返回数组
//      * @desc 冗余的'json_error_array'的同名方法
//      * @param  mixed $msg  
//      * @param  mixed  $data 
//      * @param  mixed $code 
//      */
//     function json_error_arr(
//         $msg = TipsConstant::FAIL,
//         $data = [],
//         $errors = [],
//         $code = -1
//     ) {
//         return [
//             'code'   => $code,
//             'status' => false,
//             'msg'    => $msg,
//             'res'    => $data,
//             'errors' => $errors,
//         ];
//     }
// }

if (!function_exists('json_error_array')) {
    /**
     * API错误的返回信息-返回数组 
     * @param  mixed $msg  
     * @param  mixed  $data 
     * @param  mixed $code 
     */
    function json_error_array(
        $msg = TipsConstant::FAIL,
        $data = [],
        $errors = [],
        $code = -1
    ) {
        return [
            'code'   => $code,
            'status' => false,
            'msg'    => $msg,
            'res'    => $data,
            'errors' => $errors,
        ];
    }
}

if (!function_exists('request')) {
    /**
     * Get an instance of the current request or an input item from the request.
     * 兼容低版本的lumen 例如5.1
     * @param  string  $key
     * @param  mixed   $default
     * @return \Illuminate\Http\Request|string|array
     */
    function request($key = null, $default = null)
    {
        if (is_null($key)) {
            return app('request');
        }

        return app('request')->input($key, $default);
    }
}

if (!function_exists('isTestEnv')) {
    /**
     * 是否测试环境
     */
    function isTestEnv(): bool
    {
        ## .env 文件校验
        $env = env('APP_ENV', EnvConstant::APP_ENV_DEV);
        return ($env == EnvConstant::APP_ENV_SIT) ? true : false;
    }
}

if (!function_exists('isLocalEnv')) {
    /**
     * 是否local环境
     */
    function isLocalEnv(): bool
    {
        ## .env 文件校验
        $env = env('APP_ENV', EnvConstant::APP_ENV_DEV);
        return ($env == EnvConstant::APP_ENV_LOCAL) ? true : false;
    }
}

if (!function_exists('isProdEnv')) {
    /**
     * 是否prod环境
     */
    function isProdEnv(): bool
    {
        ## .env 文件校验
        $env = env('APP_ENV', EnvConstant::APP_ENV_DEV);
        return (in_array($env, [EnvConstant::APP_ENV_PRD, EnvConstant::APP_ENV_PRODUCTION])) ? true : false;
    }
}

if (!function_exists('isUatEnv')) {
    /**
     * 是否uat环境
     */
    function isUatEnv(): bool
    {
        ## .env 文件校验
        $env = env('APP_ENV', EnvConstant::APP_ENV_DEV);
        return ($env == EnvConstant::APP_ENV_UAT) ? true : false;
    }
}

/**
 * 兼容升级到php7.2后each被剔除
 */
if (!function_exists('each')) {
    function each(&$array)
    {
        $res = array();
        $key = key($array);
        if ($key !== null) {
            next($array);
            $res[1] = $res['value'] = $array[$key];
            $res[0] = $res['key'] = $key;
        } else {
            $res = false;
        }
        return $res;
    }
}


#
