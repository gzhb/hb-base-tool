<?php

namespace Gzhb\HbBaseTool\HbConstant;

/**
 * 常量 : http状态码
 */
class HttpCodeConstant
{

    /**
     * 200 成功
     */
    const CODE_200 = 200;

    /**
     * 403 拒绝
     */
    const CODE_403 = 403;

    /**
     * 404 资源找不到
     */
    const CODE_404 = 404;

    /**
     * 405 请求方法不正确(非法的请求方式)
     */
    const CODE_405 = 405;

    /**
     * 500 服务器端出错了
     */
    const CODE_500 = 500;

    /**
     * 1045 无法访问数据库，可能是由于用户名或密码错误导致
     */
    const CODE_1045 = 1045;

    /**
     * 1054 字段不存在
     */
    const CODE_1054 = 1054;

    /**
     * 2002 无法连接到MySQL服务器，可能是由于服务器地址或端口号设置不正确导致的
     */
    const CODE_2002 = 2002;

    /**
     * 2003 数据库连接超时
     */
    const CODE_2003 = 2003;

    /**
     * 2006 MySQL服务器断开连接
     */
    const CODE_2006 = 2006;

    /**
     * 生产环境里不直接展示的错误码列表
     */
    const RESTRAIN_IN_PROD_HTTP_CODE_LISTS = [
        self::CODE_500, // 服务器遇到了意料不到的情况 Internal Server Error
        self::CODE_1045, // 无法访问数据库，可能是由于用户名或密码错误导致
        self::CODE_1054, // 字段不存在
        self::CODE_2002, // 无法连接到MySQL服务器，可能是由于服务器地址或端口号设置不正确导致的
        self::CODE_2003, // 数据库连接超时
        self::CODE_2006, // MySQL服务器断开连接

    ];


    #
}
