<?php

namespace Gzhb\HbBaseTool\HbConstant;

/**
 * 常量 : 环境key
 */
class EnvConstant
{

    /**
     * 环境列表
     */
    const APP_ENV_LIST = [
        self::APP_ENV_LOCAL,
        self::APP_ENV_DEV,
        self::APP_ENV_SIT,
        self::APP_ENV_PRD,
    ];

    /**
     * 本地开发环境(本地开发环境)
     */
    const APP_ENV_LOCAL = 'local';

    /**
     * dev开发环境(线上开发环境)
     */
    const APP_ENV_DEV = 'dev';

    /**
     * test环境(线上测试环境)
     */
    const APP_ENV_SIT = 'test';

    /**
     * uat环境(线上测试环境)
     */
    const APP_ENV_UAT = 'uat';

    /**
     * prd环境(正式环境)
     */
    const APP_ENV_PRD = 'prod';

    /**
     * prd环境(正式环境) , 旧的兼容值
     */
    const APP_ENV_PRODUCTION = 'production';

    #
}
