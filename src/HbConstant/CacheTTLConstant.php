<?php

namespace Gzhb\HbBaseTool\HbConstant;

/**
 * 常量 : 缓存失效时间
 */
class CacheTTLConstant
{

    /**
     * 缓存时间: 5秒的秒数
     */
    const SECOND_FIVE = 5;

    /**
     * 缓存时间: 10秒的秒数
     */
    const SECOND_TEN = 10;

    /**
     * 缓存时间: 20秒的秒数
     */
    const SECOND_TWENTY = 20;

    /**
     * 缓存时间: 30秒的秒数
     */
    const SECOND_THIRTY = 30;

    /**
     * 缓存时间: 60秒的秒数
     */
    const SECOND_MINUTE = 60;

    /**
     * 缓存时间: 1小时的秒数
     */
    const SECOND_HOUR = 3600;

    /**
     * 缓存时间: 1天的秒数
     */
    const SECOND_DAY = 86400;

    /**
     * 缓存时间: 1周的秒数
     */
    const SECOND_WEEK = 604800;

    /**
     * 缓存时间: 31天的秒数
     */
    const SECOND_MONTH = 2678400;

    /**
     * 缓存时间: 1分钟 (单位:分钟)
     */
    const MINUTE_ONE = 1;

    /**
     * 缓存时间: 2分钟 (单位:分钟)
     */
    const MINUTE_TWO = 2;

    /**
     * 缓存时间: 5分钟 (单位:分钟)
     */
    const MINUTE_FIVE = 5;

    /**
     * 缓存时间: 10分钟 (单位:分钟)
     */
    const MINUTE_TEN = 10;

    /**
     * 缓存时间: 30分钟 (单位:分钟)
     */
    const MINUTE_THIRTY = 30;

    /**
     * 缓存时间: 60分钟 (单位:分钟)
     */
    const MINUTE_SIXTY = 60;

    /**
     * 缓存时间: 1天的分钟数 (单位:分钟)
     */
    const MINUTE_ONE_DAY = 1440;

    /**
     * 缓存时间: 1周的分钟数 (单位:分钟)
     */
    const MINUTE_ONE_WEEK = 10080;

    /**
     * 缓存时间: 31天的分钟数 (单位:分钟)
     */
    const MINUTE_ONE_MONTH = 44640;

    #
}
