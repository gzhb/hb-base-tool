<?php

namespace Gzhb\HbBaseTool\HbConstant;

/**
 * 常量 : 提示信息
 */
class TipsConstant
{

    /**
     * 成功文案 ok
     */
    const OK = 'ok';

    /**
     * 错误文案 error
     */
    const ERROR = 'error';

    /**
     * 失败文案 fail
     */
    const FAIL = 'fail';

    /**
     * 遇到未知错误
     */
    const UNKNOW_ERROR_TEXT = '遇到未知错误.';

    /**
     * 网络连接异常
     */
    const NETWORK_ERROR_TEXT = '网络连接异常.';

    /**
     * 出错了
     */
    const ERROR_PRE = '出错了.';

    /**
     * 保存时出错了
     */
    const SAVE_ERROR = '保存时出错了.';

    /**
     * 页面不存在
     */
    const PAGE_NOT_FUND = '页面不存在.';

    /**
     * 非法的请求方式
     */
    const METHOD_NOT_ALLOWED = '非法的请求方式.';

    /**
     * 下架提示
     */
    const REMOVE_NOTICE = '下架提示:';

    /**
     * 成功加入到队列
     */
    const SUCCESS_SEND_TO_QUEUE = '成功加入到queue队列';

    /**
     * access_denied 授权被拒绝 [不能修改这个字段值,前端需要用到]
     */
    const ACCESS_DENIED = 'access_denied';

    #
}
