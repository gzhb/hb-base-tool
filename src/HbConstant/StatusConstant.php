<?php

namespace Gzhb\HbBaseTool\HbConstant;

/**
 * 常量 : 状态码
 */
class StatusConstant
{

    /**
     * 正常状态 1:是
     */
    const STATUS_YES = 1;

    /**
     * 失败状态 0:否
     */
    const STATUS_NO = 0;

    /**
     * 响应码 0:正常
     */
    const CODE_SUCCESS = 0;

    /**
     * 响应码 -1:失败
     */
    const CODE_FAIL = -1;


    #
}
