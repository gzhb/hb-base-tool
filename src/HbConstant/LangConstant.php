<?php

namespace Gzhb\HbBaseTool\HbConstant;

/**
 * 常量 : 语言包列表
 */
class LangConstant
{

    /**
     * 语言包: zh-CN
     */
    const ZH_CN = 'zh-CN';

    /**
     * 语言包: zh-HK
     */
    const ZH_HK = 'zh-HK';

    /**
     * 语言包: en
     */
    const EN = 'en';

    /**
     * 语言包: VN
     */
    const VN = 'VN';

    /**
     * int-语言包: 1:zh-CN
     */
    const ZH_CN_INT = 1;

    /**
     * int-语言包: 2:zh-HK
     */
    const ZH_HK_INT = 2;

    /**
     * int-语言包: 3:en
     */
    const EN_INT = 3;

    /**
     * int-语言包: 4:VN
     */
    const VN_INT = 4;


    #
}
