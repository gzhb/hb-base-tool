<?php

namespace Gzhb\HbBaseTool\HbConstant;

/**
 * 常量 : 分页
 */
class PageConstant
{

    /**
     * 默认页: 第1页
     */
    const FIRST = 1;

    /**
     * 默认分页条数 : 每页5条
     */
    const SIZE_5 = 5;

    /**
     * 默认分页条数 : 每页10条 (默认值)
     */
    const SIZE = 10;

    /**
     * 默认分页条数 : 每页20条
     */
    const SIZE_20 = 20;

    /**
     * 默认分页条数 : 每页50条
     */
    const SIZE_50 = 50;

    /**
     * 默认分页条数 : 每页100条
     */
    const SIZE_100 = 100;

    /**
     * 默认分页条数 : 每页200条
     */
    const SIZE_200 = 200;

    /**
     * 默认分页条数 : 每页500条
     */
    const SIZE_500 = 500;

    #
}
