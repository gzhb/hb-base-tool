<?php

namespace Gzhb\HbBaseTool\HbConstant;

/**
 * 常量 : 超时时间
 */
class TimeoutConstant
{

    /**
     * curl请求超时时间: 2秒
     */
    const CURL_TIMEOUT_2_SECOND = 2;

    /**
     * curl请求超时时间: 5秒
     */
    const CURL_TIMEOUT_5_SECOND = 5;

    /**
     * curl请求超时时间: 10秒
     */
    const CURL_TIMEOUT_10_SECOND = 10;

    /**
     * curl请求超时时间: 20秒
     */
    const CURL_TIMEOUT_20_SECOND = 20;

    /**
     * curl请求超时时间: 30秒
     */
    const CURL_TIMEOUT_30_SECOND = 30;

    /**
     * curl请求超时时间: 60秒
     */
    const CURL_TIMEOUT_60_SECOND = 60;

    /**
     * php脚本超时 60秒
     */
    const SET_TIME_LIMIT_60_SECOND = 60;

    /**
     * php脚本超时 800秒
     */
    const SET_TIME_LIMIT_800_SECOND = 800;

    /**
     * php脚本超时 0:无限制秒
     */
    const SET_TIME_LIMIT_NO = 0;


    #
}
