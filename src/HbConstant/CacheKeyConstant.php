<?php

namespace Gzhb\HbBaseTool\HbConstant;

/**
 * 常量 : 缓存键
 */
class CacheKeyConstant
{

    /**
     * 前缀(防止有数字开头的key)
     */
    const PRE = 'ck';

    /**
     * 缓存键: '职位最新更新时间的时间戳'
     */
    const LAST_DESIGNATION_UP_TIME = 'designation:last-up-time';

    /**
     * 缓存键: '获取审批用的员工与归属单位信息'
     */
    const ATTENDANCE_APPROVE_COUNT_EMPLOYEE = 'approve:count-employee-detail-info';

    /**
     * 缓存键: '获取所有非草稿状态的审批单的批次ID'
     */
    const APPROVEL_VALID_BATCH_IDS = 'approvel:valid-batch-ids';

    /**
     * 缓存键: '需要我审批的流程'
     */
    const APPROVE_UN_APPROVE_LIST = 'approve:un-approve-list';


    /**
     * 缓存键: '未审批的数量'
     */
    const APP_APPROVEL_COUNT_NUM = 'approvel:app-unread-count-num';

    /**
     * 缓存键: '公司所有部门'
     */
    const COMPANY_ALL_DEPARTMENT = 'department:all-list';

    /**
     * 缓存键: '部门变更时间戳'
     */
    const LAST_DEPARTMENT_CHANGE_TIME = 'department:last-change-time';

    /**
     * 缓存键: '管理员基本信息与权限'
     */
    const ADMIN_PERMISSION_INFO = 'admin:permission-info';

    /**
     * 缓存键: '授权token密码校验类型'
     */
    const OAUTH_GRANT_TYPE_PASSWORD = 'oauth:grant-type-password';

    /**
     * 缓存键: '用户信息'
     */
    const OAUTH_USER_INFO = 'oauth:user-info';

    /**
     * 缓存键: '员工的简要uuid信息'
     */
    const EMPLOYEE_SAMPLE_UUID_INFO = 'employee:sample-uuid-info';

    /**
     * 缓存键: '员工相关的考勤规则配置'
     */
    const ATTENDANCE_EMPLOYEE_CONFIG_BASE = "attendance:employee-config-base";

    /**
     * 缓存键: '统计公司考勤分组数目'
     */
    const ATTENDANCE_COMPANY_GROUP_NUM = "attendance:company-group-num";

    /**
     * 缓存键: '公司顶部菜单'
     */
    const COMPANY_TOP_MENU = "company:top-menu";

    /**
     * 缓存键: '公司的自定义时选择的功能'
     */
    const COMPANY_BASE_IDS = "company:base-ids";

    /**
     * 缓存键: '公司名称'
     */
    const COMPANY_NAME = "company:name";

    /**
     * 缓存键: 'account应用的用户信息'
     */
    const ACCOUNT_USER_INFO = "account:user-info";

    /**
     * 缓存键: 'oauth中间件授权-账号'
     */
    const OAUTH_MIDDLEWARE_ACCOUNT = "oauth:middleware-account";

    /**
     * 缓存键: 'oauth中间件授权-用户'
     */
    const OAUTH_MIDDLEWARE_USER = "oauth:middleware-user";

    /**
     * 缓存键: '时区详情'
     */
    const DATE_TIME_ZONE_INFO = "s:datetimezone-info";

    /**
     * 缓存键: '班次详情'
     */
    const ATTENDANCE_CLASS_INFO = "attendance:class";


    #
}
